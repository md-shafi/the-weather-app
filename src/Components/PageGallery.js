import React from 'react'
import { locale } from './../locale'

export default function PageGallery() {
    return (
        <div className="images-container">
            <div className="img image-container-left">
                Image 1
            </div>
            <div className="image-container-right">
                <div className="image-with-text">
                    <div className="img img-2">
                        Image 2
                    </div>
                    <p>{locale.consequatNulla}</p>
                </div>
                <div className="image-with-text">
                    <div className="img img-3">
                        Image 3
                    </div>
                    <p>{locale.nonIrure}</p>
                </div>
            </div>
        </div>
    )
}
