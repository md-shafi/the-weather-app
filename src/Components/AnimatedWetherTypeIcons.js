import React from 'react'
import './../AnimWetherIcons.css'

export default function AnimatedWetherTypeIcons({ icon }) {
    return (
        <span className="anim-icon-container">
            {icon === 'cloudy' &&
                <div className="icon cloudy">
                    <div className="cloud"></div>
                    <div className="cloud"></div>
                </div>
            }

            {icon === 'flurries' &&
                <div className="icon flurries">
                    <div className="cloud"></div>
                    <div className="snow">
                        <div className="flake"></div>
                        <div className="flake"></div>
                    </div>
                </div>
            }

            {icon === 'sunny' &&
                <div className="icon sunny">
                    <div className="sun">
                        <div className="rays"></div>
                    </div>
                </div>
            }

            {icon === 'rainy' &&
                <div className="icon rainy">
                    <div className="cloud"></div>
                    <div className="rain"></div>
                </div>
            }
        </span>
    )
}
