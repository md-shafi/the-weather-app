import React, { useState, useEffect } from 'react';
import { useThemeContext } from './../Contexts/ThemeContextProvider'

export default function TemperatureBar(props) {
    const theme = useThemeContext();

    const [tempBarHeight, setTempBarHeight] = useState(null)
    const [hovered, setHovered] = useState(false)
    const [currentTempVisible, setCurrentTempVisible] = useState(false)

    function calculateBarheight(maxTemp, temp) {

        // increase temperature bar height from its original height to 70% of the container height to make it easy to compare
        let adjustHeightProportion = (70 - maxTemp) / maxTemp;
        return `${temp + temp * adjustHeightProportion}%`
    }

    // set temperature bar color to #BDC3C7 if it is below 0
    function setBarBackgroundColor(temp) {
        if (temp < 0) return '#BDC3C7'
    }

    useEffect(() => {
        setTempBarHeight(calculateBarheight(Math.abs(props.maxTemp), Math.abs(props.temp.temperature)))
    }, [props.maxTemp, props.temp.temperature])


    // get current time to display current weather info
    const d = new Date();
    var currentHour = d.getHours().toString();
    currentHour = currentHour.length < 2 ? '0' + currentHour : currentHour

    useEffect(() => {
        if (props.temp.time.substr(11, 2) === currentHour) {
            setCurrentTempVisible(true)
        } else {
            setCurrentTempVisible(false)
        }
    }, [])

    function handleMouseEnter() {
        setHovered(true)
    }

    function handleMouseLeave() {
        setHovered(false)
    }

    function handleTempBarInfoVisibility() {
        return (
            !hovered && currentTempVisible ? ' show' :
                hovered && !currentTempVisible ? ' show' :
                    !hovered && !currentTempVisible ? ' hide' :
                        hovered && currentTempVisible ? ' show' : ' hide'
        )
    }

    return (
        <div className="temp-bar-container">
            <div
                className={"time " + handleTempBarInfoVisibility()}>
                {props.temp.time.substr(11, 5)}
            </div>
            <div
                className={"team-bar" + (theme.fancyTheme ? " bg-light-blue bar-anim" : " bg-white")}
                style={{
                    height: tempBarHeight,
                    backgroundColor: setBarBackgroundColor(props.temp.temperature)
                }}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
            >
            </div>
            <div className={"temp " + handleTempBarInfoVisibility()}>
                {props.temp.temperature}&#176;
            </div>
            <div className={"type " + handleTempBarInfoVisibility()}>
                {props.temp.type}
            </div>
        </div>
    )
}
