import React from 'react'
import { useThemeUpdateContext } from '../Contexts/ThemeContextProvider'

export default function ThemeUpdateButtons() {
    const updateTheme = useThemeUpdateContext();

    return (
        <div className="btn-container">
            <button className="btn btn-primary"
                onClick={() => updateTheme({ defaultTheme: false, fancyTheme: true })}
            >
                CHANGE THEME
            </button>
            <button className="btn btn-white"
                onClick={() => updateTheme({ defaultTheme: true, fancyTheme: false })}
            >
                RESET
            </button>
        </div>
    )
}