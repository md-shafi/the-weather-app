import React from 'react'
import TemperatureBars from './TemperatureBars'
import AverageTemperature from './AverageTemperature'
import MinMaxTemperature from './MinMaxTemperature'
import { useThemeContext } from './../Contexts/ThemeContextProvider'

export default function GraphContainer({ weatherData }) {
    const theme = useThemeContext();
    const themeClassName = theme.fancyTheme ? " fadeIn" : " default-theme"

    return (
        <div className={"graph-container" + themeClassName}>
            <div>
                <AverageTemperature entries={weatherData.entries} />
            </div>
            <TemperatureBars className="temp-bars"
                weatherData={weatherData}
            />
            <MinMaxTemperature className="temp-bars"
                minTemperature={weatherData.minTemperature}
                maxTemperature={weatherData.maxTemperature}
            />
        </div>
    )
}
