import React from 'react';
import Header from './Components/Header'
import GraphContainer from './Components/GraphContainer'
import { ThemeProvider } from './Contexts/ThemeContextProvider'
import ThemeUpdateButtons from './Components/ThemeUpdateButtons'
import PageMainTextContent from './Components/PageMainTextContent'
import PageGallery from './Components/PageGallery'

import './App.css';
import { locale } from './locale'

import useAxios from './Hooks/useAxios'
import Loader from './Components/Loader'
import PageNotFound from './Components/PageNotFound'

function App() {
    const fetchedData = useAxios();

    let content = null

    if (fetchedData.loading) {
        content = <Loader />
    }

    if (fetchedData.error) {
        content = <PageNotFound error={fetchedData.error} />
    }

    if (fetchedData.data) {
        var weatherData = fetchedData.data.me.home.weather;

        content =
            <>
                <Header title={locale.companyName} />
                <main>
                    <ThemeProvider>
                        <GraphContainer weatherData={weatherData} />
                        <ThemeUpdateButtons />
                    </ThemeProvider>
                    <PageMainTextContent />
                    <PageGallery />
                </main>
            </>
    }

    return (
        <div className="App">
            {content}
        </div>
    );
}

export default App;
