import React from 'react'

export default function Header({ title }) {
    return (
        <header>
            <div>
                <span>{title}</span>
            </div>
        </header>
    )
}
