import React from 'react'
import AnimatedWetherTypeIcons from './AnimatedWetherTypeIcons'
import InanimatedWeatherTypeIcons from './InanimatedWeatherTypeIcons'
import { useThemeContext } from './../Contexts/ThemeContextProvider'


export default function MostProbableWeatherType({ entries }) {
    const theme = useThemeContext();

    // get the most probable weather type
    function getMostFrequentEl(arr) {
        if (arr.length === 0)
            return null;
        var modeMap = {};
        var maxEl = arr[0], maxCount = 1;
        for (var i = 0; i < arr.length; i++) {
            var el = arr[i].type;
            if (modeMap[el] == null)
                modeMap[el] = 1;
            else
                modeMap[el]++;
            if (modeMap[el] > maxCount) {
                maxEl = el;
                maxCount = modeMap[el];
            }
        }
        return maxEl;
    }

    // returns the weather type icons 
    function getWeatherTypeIcon(type) {
        switch (type) {
            case 'sun':
                return 'sunny'
            case 'cloud':
                return 'cloudy'
            case 'rain':
                return 'rainy'
            case 'snow':
                return 'flurries'
            default:
                return undefined
        }
    }

    const propableWeatherType = getMostFrequentEl(entries)
    const weaterTypeIcon = getWeatherTypeIcon(propableWeatherType)

    return (
        <div>
            { weaterTypeIcon && !theme.fancyTheme && <InanimatedWeatherTypeIcons icon={weaterTypeIcon} />}
            { weaterTypeIcon && theme.fancyTheme && <AnimatedWetherTypeIcons icon={weaterTypeIcon} />}
            { !weaterTypeIcon && <span className="weather-type-text"> {propableWeatherType} </span>}
        </div>
    )
}
