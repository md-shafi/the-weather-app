import React, { useState, useEffect } from 'react'
import PageNotFound from './PageNotFound'
import './../Loader.css'


export default function Loader() {
    const [hideLoader, setHideLoader] = useState(false)

    // hide loader after 10 sec to eventually show the PageNotFound
    useEffect(() => {
        let timer = setTimeout(() => setHideLoader(true), 10000)

        return () => {
            clearTimeout(timer)
        }
    }, [])

    return (
        <>
            {!hideLoader && <div className="loader"></div>}
            {hideLoader && <PageNotFound />}
        </>
    )
}