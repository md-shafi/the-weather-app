import React from 'react';
import TemperatureBar from './TemperatureBar'

export default function TemperatureBars({ weatherData }) {
    return (
        <div className="temp-bars">
            {
                weatherData.entries.map(entry => {
                    return <TemperatureBar key={entry.time} temp={entry} maxTemp={weatherData.maxTemperature} />
                })
            }
        </div>
    )
}