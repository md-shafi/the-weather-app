import React from 'react'
import { locale } from './../locale'
import lost from './../images/lost.svg'


export default function PageNotFound() {
    return (
        <div className="error-page">
            <img src={lost} alt="page not found" />
            <span>
                {locale.pageNotFound}
            </span>
        </div>
    )
}
