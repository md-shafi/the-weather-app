import React, { useState, useContext } from 'react'

const ThemeContext = React.createContext()
const ThemeUpdateContext = React.createContext()

export function useThemeContext() {
    return useContext(ThemeContext)
}

export function useThemeUpdateContext() {
    return useContext(ThemeUpdateContext)
}

export function ThemeProvider({ children }) {
    const [theme, setTheme] = useState({
        defaultTheme: true,
        fancyTheme: false
    })

    function updateTheme(theme) {
        setTheme(theme)
    }

    return (
        <ThemeContext.Provider value={theme}>
            <ThemeUpdateContext.Provider value={updateTheme}>
                {children}
            </ThemeUpdateContext.Provider>
        </ThemeContext.Provider>
    )
}
