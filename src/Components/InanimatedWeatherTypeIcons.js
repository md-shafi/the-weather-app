import React from 'react'
import sunnyIcon from './../images/icon-sunny-50.png'
import cloudyIcon from './../images/icon-cloudy-50.png'
import rainyIcon from './../images/icon-rainy-50.png'
import snowyIcon from './../images/icon-snowy-50.png'

export default function InanimatedWeatherTypeIcons({ icon }) {
    return (
        <img className="icon-inanimated"
            src={
                icon === 'sunny' ? sunnyIcon :
                    icon === 'cloudy' ? cloudyIcon :
                        icon === 'rainy' ? rainyIcon :
                            icon === 'flurries' ? snowyIcon : ''
            }
            alt="weather type icon"
        />
    )
}
