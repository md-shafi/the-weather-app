import { useState, useEffect } from 'react'
import Axios from 'axios'

export default function useAxios() {
    const [response, setResponse] = useState({
        loading: false,
        data: null,
        error: false
    })

    useEffect(() => {
        const BASE_URL = "https://my-json-server.typicode.com/notachance/weather-forcast/data"

        setResponse({
            loading: true,
            data: null,
            error: false
        });

        async function fetchData() {
            const response = await Axios({
                url: BASE_URL,
                method: 'get',
                headers: { 'Content-Type': 'application/json' },
            })

            return response.data
        }

        async function fetchWeatherData() {
            try {
                const weatherData = await fetchData()
                setResponse({
                    loading: false,
                    data: weatherData,
                    error: false
                })
            } catch (err) {
                setResponse({
                    loading: false,
                    data: null,
                    error: err
                })
            }
        }

        fetchWeatherData()

    }, [])

    return response
}


