import React from 'react'
import MostProbableWeatherType from './MostProbableWeatherType'
import { useThemeContext } from './../Contexts/ThemeContextProvider'

export default function AverageTemperature({ entries }) {
    const theme = useThemeContext();
    const avgTempTextThemeClassName = theme.fancyTheme ? " avg-temp-text-anim" : ""
    const avgTempThemeClassName = theme.fancyTheme ? " avg-temp-anim" : ""

    // calculate average temperature
    const averageTemperature = Math.round(entries.reduce((total, entry) => total + entry.temperature, 0) / entries.length)

    return (
        <div>
            <div className={"avg-temp-text" + avgTempTextThemeClassName}> Average temparature / Mostly </div>
            <div className={"avg-temp-value" + avgTempThemeClassName}>
                {averageTemperature}&#176;<MostProbableWeatherType entries={entries} />
            </div>
        </div>
    )
}