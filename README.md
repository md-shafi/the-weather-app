### Weather App (React, CSS)
A weather app that fetches weather forecast data for 24 hours and maps the data to a bar chart. The app displays information such as current temperature and weather type, average temperature, average weather type, highest and lowest temperature, Users can chose between a dark theme (default) and coloured theme with animated CSS icons. The app has a responsive design and contains one image place holder (page wide) and two cards with image place holders and some text.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.