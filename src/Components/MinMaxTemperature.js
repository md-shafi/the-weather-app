import React from 'react'
import { useThemeContext } from './../Contexts/ThemeContextProvider'

export default function MinMaxTemperature({ minTemperature, maxTemperature }) {
    const theme = useThemeContext();
    const themeClassName = theme.fancyTheme ? " min-max-temp-anim" : ""

    return (
        <div className={"min-max-temp" + themeClassName}>
            <span>High / Low</span> <span>&nbsp;{maxTemperature}&#176; / {minTemperature}&#176;</span>
        </div>
    )
}
