export const locale = {
    companyName: "AW Company",
    pageHeading: "Infrastructure Supply chain seed lean startup technology",
    eaExcepteur: " Ea excepteur dolore consequat et ut. Nostrud nisi ea sunt duis. Ut ut sint consectetur ea do deserunt laborum sunt reprehenderit proident esse id. Consequat nulla et voluptate labore consequat. Non irure ex ipsum anim amet.",
    nostrudNisi: "Nostrud nisi ea sunt duis. Ut ut sint consectetur ea do deserunt laborum sunt reprehenderit proident esse id. Consequat nulla et voluptate labore consequat.",
    consequatNulla: "Consequat nulla et voluptate consequat.",
    nonIrure: "Non irure ex ipsum anim amet.",
    pageNotFound: "Looks like you'r lost 🙈. To get help ask for Master Jacob. May the force be with you!",
}