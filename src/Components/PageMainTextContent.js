import React from 'react'
import { locale } from './../locale'

export default function PageMainTextContent(props) {
    return (
        <div className="page-text-container">
            <h1 className="page_heading">{locale.pageHeading}</h1>
            <p>{locale.eaExcepteur}</p>
            <p>{locale.nostrudNisi}</p>
        </div>
    )
}
